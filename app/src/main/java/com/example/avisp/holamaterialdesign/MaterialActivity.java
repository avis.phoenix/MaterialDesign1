package com.example.avisp.holamaterialdesign;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MaterialActivity extends AppCompatActivity {

    public final static String EXTRA_NAME = "com.example.avisp.holamaterialdesign.NOMBRE";
    public final static String EXTRA_COLOR = "com.example.avisp.holamaterialdesign.COLOR";

    private static final String TRANSITION_FAB = "fab_transition";
    //La lista de nombre y colores que vamos a tener
    private String[] nombres;
    private int[] colores;

    private List<Card> cardList; //Los datos de las tarjetas

    //Manejamos el recycleview
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CardViewAdapter mAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_material);

        //Cargamos los nombres y colores por default
        nombres = getResources().getStringArray(R.array.nombres_iniciales);
        colores = getResources().getIntArray(R.array.colores_iniciales);

        initCards();

        mRecyclerView = (RecyclerView) findViewById(R.id.vistaReciclada);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        //Agregamos el manejador de las tarjetas:
        if (mAdapter == null)
            mAdapter = new CardViewAdapter(cardList,this);
        mRecyclerView.setAdapter(mAdapter);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Log.d("MaterialActivity","onCreate");

    }

    private void initCards(){
        if (cardList == null){
            cardList = new ArrayList<>();
        }else{
            cardList.clear();
        }

        for(int i =0; i < nombres.length; i++){
            Card item = new Card();
            item.setId((long)i);
            item.setColor(colores[i]);
            item.setName(nombres[i]);
            cardList.add(item);
        }
    }

    public void addFriend(View view){
        Pair<View, String> pair = Pair.create(view.findViewById(R.id.addFAB), TRANSITION_FAB);

        ActivityOptionsCompat options;
        options = ActivityOptionsCompat.makeSceneTransitionAnimation(this,pair);

        Intent transitionIntent = new Intent(this, addCardActivity.class);
        startActivityForResult(transitionIntent, mAdapter.getItemCount(), options.toBundle());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(resultCode, resultCode, data);

        String name;
        int color;

        if (requestCode == mAdapter.getItemCount()){
            if (resultCode == RESULT_OK){
                name = data.getStringExtra(EXTRA_NAME);
                color = data.getIntExtra(EXTRA_COLOR,0);

                mAdapter.addCard(name, color);
            }
        } else{
            //Estamos editando
            if (resultCode == RESULT_OK){
                name = data.getStringExtra(EXTRA_NAME);
                RecyclerView.ViewHolder viewHolder = mRecyclerView.findViewHolderForAdapterPosition(requestCode);
                viewHolder.itemView.setVisibility(View.INVISIBLE);
                mAdapter.updateCard(name,requestCode);
            }
        }
    }

    public void doSmoothScroll(int position){ mRecyclerView.smoothScrollToPosition(position); }
}
