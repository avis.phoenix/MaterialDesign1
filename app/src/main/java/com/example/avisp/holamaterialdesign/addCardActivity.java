package com.example.avisp.holamaterialdesign;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class addCardActivity extends AppCompatActivity {

    private EditText nameEditText;
    private TextView initialTextView;
    private int color;
    private Intent intent;
    private Random randomGenerator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        randomGenerator = new Random();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        nameEditText = (EditText) findViewById(R.id.nameTxt);
        initialTextView = (TextView) findViewById(R.id.newInitialLbl);

        intent = getIntent();
        int[] colores = getResources().getIntArray(R.array.colores_iniciales);
        color = colores[randomGenerator.nextInt(colores.length)];

        initialTextView.setText("");
        initialTextView.setBackgroundColor(color);

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (count == 0){
                    initialTextView.setText("");
                }
                else if (count == 1){
                    initialTextView.setText(String.valueOf(charSequence.charAt(0)));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void finishFriend(View v){
        String nombre = nameEditText.getText().toString().trim();
        if (nombre.isEmpty()){
            MessageBox mensajeError = new MessageBox(this,getString(R.string.errormesssage_1),getString(R.string.errortitle_1));
            mensajeError.getDialog().show();
        }else{
            intent.putExtra(MaterialActivity.EXTRA_NAME, nombre);
            intent.putExtra(MaterialActivity.EXTRA_COLOR, color);

            setResult(RESULT_OK,intent);
            supportFinishAfterTransition();
        }
    }
}
