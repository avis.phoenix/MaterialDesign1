package com.example.avisp.holamaterialdesign;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class editCardActivity extends AppCompatActivity {
    private EditText nameEditText;
    private TextView initialTextView;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String name;
        int color;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_card);

        intent = getIntent();
        name = intent.getStringExtra(MaterialActivity.EXTRA_NAME);
        color = intent.getIntExtra(MaterialActivity.EXTRA_COLOR,0);

        nameEditText = (EditText) findViewById(R.id.editNameTxt);
        initialTextView = (TextView) findViewById(R.id.editInitialLbl);

        if (!name.isEmpty()){
            initialTextView.setText(String.valueOf(name.charAt(0)));
        }else{
            initialTextView.setText("");
        }

        initialTextView.setBackgroundColor(color);
    }

    public void saveFriend(View v){
        String nombre = nameEditText.getText().toString().trim();
        if (nombre.isEmpty()){
            MessageBox mensajeError = new MessageBox(this,getString(R.string.errormesssage_1),getString(R.string.errortitle_1));
            mensajeError.getDialog().show();
        }else{
            intent.putExtra(MaterialActivity.EXTRA_NAME, nombre);

            setResult(RESULT_OK,intent);
            supportFinishAfterTransition();
        }
    }

    public void delFriend(View v){
    }
}
