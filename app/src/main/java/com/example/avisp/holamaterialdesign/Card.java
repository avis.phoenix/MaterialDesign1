package com.example.avisp.holamaterialdesign;

/**
 * Created by avisp on 30/09/2017.
 */

public class Card {
    private long id;
    private String name;
    private int color;

    public long getId() { return this.id; }

    public void setId(long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public int getColor() { return color; }

    public void setColor(int color) { this.color = color; }
}
