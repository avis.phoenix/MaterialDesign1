package com.example.avisp.holamaterialdesign;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.BoolRes;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by avisp on 01/10/2017.
 */

public class MessageBox {
    private String okText, cancelText, messageText, titleText;
    private DialogInterface.OnClickListener functionGo;
    private Boolean hasOK, hasCancel;
    private Activity act;
    private AlertDialog.Builder builder;

    public MessageBox(Activity activity, String msg, String title) {
        act = activity;
        hasOK = false;
        hasCancel = true;
        functionGo = null;

        messageText = msg;
        titleText = title;

        initializeVars();

        buildMessageBox();
    }

    public MessageBox(Activity activity, String msg, String title, DialogInterface.OnClickListener okFunction) {
        act = activity;
        hasOK = true;
        hasCancel = true;
        functionGo = okFunction;

        messageText = msg;
        titleText = title;

        initializeVars();

        buildMessageBox();
    }

    public MessageBox(Activity activity, String msg, String title, Boolean cancelButton, DialogInterface.OnClickListener okFunction) {
        act = activity;
        hasOK = true;
        hasCancel = cancelButton;
        functionGo = okFunction;

        messageText = msg;
        titleText = title;

        initializeVars();

        buildMessageBox();
    }

    public MessageBox(Activity activity, String msg, String title, String okLabel, Boolean okButton, String cancelLabel, Boolean cancelButton, DialogInterface.OnClickListener okFunction) {
        act = activity;
        hasOK = okButton;
        hasCancel = cancelButton;
        functionGo = okFunction;

        messageText = msg;
        titleText = title;

        okText = okLabel;
        cancelText = cancelLabel;

        buildMessageBox();
    }

    private void buildMessageBox(){
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(act);
        builder.setTitle(titleText);

        if (hasOK && hasCancel){
            builder.setMessage( messageText)
                    .setPositiveButton( okText , functionGo)
                    .setNegativeButton( cancelText, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        else if (hasCancel){
            builder.setMessage( messageText)
                    .setNegativeButton( cancelText, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
        }
        else if (hasOK){
            builder.setMessage( messageText)
                    .setPositiveButton( okText , functionGo);
        }
    }

    private void initializeVars(){
        cancelText = act.getString(R.string.cancel_txt);
        okText = act.getString(R.string.accept_txt);

    }

    public AlertDialog getDialog(){ return builder.create(); }

}
