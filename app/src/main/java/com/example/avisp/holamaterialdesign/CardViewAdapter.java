package com.example.avisp.holamaterialdesign;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by avisp on 30/09/2017.
 */

public class CardViewAdapter extends RecyclerView.Adapter<CardViewAdapter.CardHolder>{
    private List<Card> cardsDataList;
    private Context contexto;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class CardHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView initialLbl;
        public TextView nameLbl;
        public Button deleteBtn;

        public CardHolder(View itemView){

            super(itemView);

            initialLbl = itemView.findViewById(R.id.initialLbl);
            nameLbl = itemView.findViewById(R.id.nameLbl);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);

            deleteBtn.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    Log.d("CardHolder","constructor: click delete button in pos:" + String.valueOf(getAdapterPosition()));
                    animateCircularDelete(v,getAdapterPosition());
                }
            });
        }
    }

    public CardViewAdapter(List<Card> cartas, Context context){
        cardsDataList = cartas;
        contexto = context;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(parent.getContext());
        View v = li.inflate(R.layout.cardview_holder,parent, false);
        return new CardHolder(v);
    }

    @Override
    public void onBindViewHolder(CardHolder holder, int position) {
        String name = cardsDataList.get(position).getName();
        holder.initialLbl.setText(Character.toString(name.charAt(0)));
        holder.nameLbl.setText(name);
        holder.initialLbl.setBackgroundColor(cardsDataList.get(position).getColor());

    }

    @Override
    public int getItemCount() {
        if (cardsDataList.isEmpty())
            return 0;
        else
            return cardsDataList.size();
    }

    @Override
    public long getItemId(int position){ return cardsDataList.get(position).getId(); }

    @Override
    public void onViewDetachedFromWindow(CardHolder holder){
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(CardHolder holder){
        super.onViewAttachedToWindow(holder);
        animateCircularReveal(holder.itemView);
    }

    public void animateCircularReveal(View v){
        int centerX, centerY, startRadius, endRadius;
        Animator animation;

        centerX = 0; centerY = 0;
        startRadius = 0; endRadius = Math.max(v.getHeight(), v.getWidth());

        animation = ViewAnimationUtils.createCircularReveal(v,centerX,centerY,startRadius,endRadius);

        v.setVisibility(View.VISIBLE);

        animation.start();
    }

    public void animateCircularDelete(final View v, final int position){
        int centerX, centerY, startRadius, endRadius;
        Animator animation;

        centerX = v.getWidth(); centerY = v.getHeight();
        startRadius = Math.max(v.getHeight(), v.getWidth()); endRadius = 0;

        animation = ViewAnimationUtils.createCircularReveal(v,centerX,centerY,startRadius,endRadius);
        animation.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                v.setVisibility(View.INVISIBLE);
                cardsDataList.remove(position);
                notifyItemRemoved(position);
            }
        });

        //v.setVisibility(View.VISIBLE);

        animation.start();
    }

    public void addCard(String name, int color){
        Card card = new Card();
        card.setName(name);
        card.setColor(color);
        card.setId(getItemCount());

        cardsDataList.add(card);
        ((MaterialActivity) contexto).doSmoothScroll(getItemCount());
        notifyItemInserted(getItemCount());

    }

    public void updateCard(String name, int position){
        cardsDataList.get(position).setName(name);
        notifyItemChanged(position);
    }

    public void deleteCard(View v, int position){
        animateCircularDelete(v, position);
    }


}
